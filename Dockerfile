FROM python:3.8-alpine

COPY exec.py /usr/local/bin/exec.py

CMD [ "/usr/local/bin/exec.py" ]
