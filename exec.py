#!/usr/bin/env python3

import json
import os
import shutil
import urllib.request


if __name__ == '__main__':

    # Get the ECS Task ID from metadata
    print(os.environ)
    f = urllib.request.urlopen(f'{os.environ["ECS_CONTAINER_METADATA_URI"]}/task')
    metadata = json.loads(f.read())

    task_id = metadata['TaskARN'].split('/')[-1]
    print(f"task_id={task_id}")

    # Create a CloudWatch Agent configuration file to extract each file
    cw_agent_config = {
        "agent": {
            "metrics_collection_interval": 10,
            "logfile": "/dev/stdout",
            "region": os.environ['AWS_REGION']
        },
        "logs": {
            "logs_collected": {
                "files": {
                    "collect_list": []
                }
            },
            "force_flush_interval": 15
        }
    }

    log_files = os.environ['LOG_FILES'].split()
    print(log_files)

    for file_path in log_files:
        file_name = os.path.basename(file_path)
        stream_name = f'{file_name}/{task_id}'
        print(f'Collecting {file_path}, streaming to {stream_name}')
        entry = {
            "file_path": file_path,
            "log_group_name": os.environ['LOG_GROUP_NAME'],
            "log_stream_name": stream_name,
            "timezone": "UTC"
        }
        cw_agent_config['logs']['logs_collected']['files']['collect_list'].append(entry)

    # We have to use shutil.copy() here because Docker has already created /etc/cwagentconfig
    # as a directory, and we need to replace it with a file which Python won't like
    with open('/opt/cwagentconfig', 'w') as f:
        json.dump(cw_agent_config, f)
    shutil.copy('/opt/cwagentconfig', '/etc/cwagentconfig')

    print("Successfully created CloudWatch Agent configuration file")
