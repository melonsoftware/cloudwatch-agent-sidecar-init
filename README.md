# Cloudwatch Agent Sidecar initialiser

## Problem

You have an application that runs in a Docker container, which you want to deploy on AWS using ECS &ndash; especially when using Fargate &ndash; and stream relevant logs to CloudWatch Logs.  The application uses [Active Logging](https://rclayton.silvrback.com/container-services-logging-with-docker) by writing useful output to log files within the container, and you can't, or don't want to, modify the container to write to stdout so you can use Docker logging to get the logs out to CloudWatch. If your ECS cluster is EC2-backed you can at least SSH into the instances and then `docker exec` into running containers to read the files, but this is clunky and not an option at all if using Fargate.

AWS publish and maintain the [CloudWatch Agent](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Install-CloudWatch-Agent.html), a Go binary which can collect both logs and metrics from servers, including containers. They even publish a [containerised version](https://hub.docker.com/r/amazon/cloudwatch-agent) of it; this can easily be run as a sidecar container to your main application, sharing `/var/log` (or wherever) between the two containers so that logs written by the application are available to the agent container to stream. However, the Agent needs to read a configuration file which tells it which log files to stream and to where. The [official advice](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ContainerInsights-build-docker-image.html) from AWS seems to be to extend their container in your own Dockerfile to add the necessary configuration of what log files you want to stream and where you want to stream them to. But this will require you to maintain and redeploy your own container whenever the logging requirements of your application change (eg if a new logfile is added). It would be nicer if that configuration could be done as part of the ECS Task Definition (ie the same place the standard awslogs logging is configured).

This container can be used as a 'sidecar to the sidecar' to configure the Cloudwatch Agent based on parameters passed at container start time.  It processes a list of log files and constructs a JSON configuration file to place at `/etc/cwagentconfig`, which is then shared with the Agent container.

## Use

Example CloudFormation snippet:

```yaml
    CloudwatchLogsGroup:
        Type: "AWS::Logs::LogGroup"
        Properties:
            LogGroupName: "/my/log/group"
            RetentionInDays: 14
            
    # This Role is shared by all containers in the task, so it needs to have permission to stream
    # to CloudWatch Logs in addition to whatever permissions you might need for the actual task
    ContainerRole:
        Type: "AWS::IAM::Role"
        Properties:
            AssumeRolePolicyDocument:
                Version: "2012-10-17"
                Statement:
                  - Effect: "Allow"
                    Principal: { Service: "ecs-tasks.amazonaws.com" }
                    Action: "sts:AssumeRole"
            Policies:
              - PolicyName: "logs"
                PolicyDocument:
                    Version: "2012-10-17"
                    Statement:
                      - Action:
                          - "logs:DescribeLogStreams"
                        Effect: "Allow"
                        Resource: "*"
                      - Action:
                          - "logs:CreateLogStream"
                          - "logs:PutLogEvents"
                        Effect: "Allow"
                        Resource:
                          - !Sub "${CloudwatchLogsGroup.Arn}"
                          - !Sub "${CloudwatchLogsGroup.Arn}:log-streams:*"
            RoleName: "my-app-role"

    TaskDefinition:
        Type: "AWS::ECS::TaskDefinition"
        Properties:
            ContainerDefinitions:
            
              - Image: "myapplicationcontainer:1.42.0"
                LogConfiguration:
                    LogDriver: "awslogs"
                    Options:
                        awslogs-group: !Ref "CloudwatchLogsGroup"
                        awslogs-region: !Ref "AWS::Region"
                        awslogs-stream-prefix: "stdout"
                MountPoints:
                    # Assuming that this application writes logs to /var/log, share that directory
                    #with the Agent container
                  - SourceVolume: "logs"
                    ContainerPath: "/var/log"
                Name: "application"
                
              - DependsOn:
                    # Don't start the logs agent until the configuration container has finished running
                  - ContainerName: "logs-init"
                    Condition: "SUCCESS"
                Image: "amazon/cloudwatch-agent"
                LogConfiguration:
                    LogDriver: "awslogs"
                    Options:
                        awslogs-group: !Ref "CloudwatchLogsGroup"
                        awslogs-region: !Ref "AWS::Region"
                        awslogs-stream-prefix: !Sub "stdout"
                MountPoints:
                  - SourceVolume: "logs"
                    ContainerPath: "/var/log"
                    # In addition to sharing the logs volume, share the configuration file with
                    # the init container
                  - SourceVolume: "cwlogsconfig"
                    ContainerPath: "/etc/cwagentconfig"
                Name: "logs"
              - Environment:
                    # In order to give the log streams sensible names, we will want to extract the 
                    # Task ID from the ECS metadata service
                  - Name: "ECS_ENABLE_CONTAINER_METADATA"
                    Value: "true"
                  - Name: "LOG_GROUP_NAME"
                    Value: !Ref "CloudwatchLogsGroup"
                  - Name: "LOG_FILES"
                    Value: >
                        /var/log/cron.log
                        /var/log/application/error.log
                        /var/log/whatever/else/accesslog
                Essential: false
                Image: "melonsoftware/cloudwatch-agent-sidecar-init:latest"
                LogConfiguration:
                    LogDriver: "awslogs"
                    Options:
                        awslogs-group: !Ref "CloudwatchLogsGroup"
                        awslogs-region: !Ref "AWS::Region"
                        awslogs-stream-prefix: !Sub "stdout"
                MountPoints:
                  - SourceVolume: "logs"
                    ContainerPath: "/var/log"
                  - SourceVolume: "cwlogsconfig"
                    ContainerPath: "/etc/cwagentconfig"
                Name: "logs-init"
            Family: "my-application-task"
            NetworkMode: "awsvpc"
            RequiresCompatibilities:
              - "FARGATE"
            Volumes:
              - Name: "logs"
              - Name: "cwlogsconfig"

```
With this setup, the file `/var/log/application/error.log` would be written to the stream `cron.log/{task_id}` in the CloudWatch logs group.  This matches the (slightly messy) structure of the streams that AWS creates for the stdout of the containers, which are written to `stdout/application/{task_id}` rather than the (perhaps more intuitive) `{task_id}/application/stdout`.